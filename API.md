API documentation
=================

Objects:

    * tasks
    * pools
    * exams
    * audits
    * corrections

Users
-----

* `GET    /users`

    return a list of users (as dict/JSON)
    
    ?q=<search terms>

* `POST   /users`

    create a new user programmatically, takes JSON as request body

* `PUT   /users/<id>`

    update a user's information, such as email or name cannot change id

* `DELETE /users/<id>`

    set the deleted flag on a user

Pools
-----

* `GET    /pools`

    return all (not-deleted) pools
    
    ?q=<search terms>
    ?all=<true|false|0|1>

* `GET    /pools/<id>`

    description: get one specific pool
        

* `POST   /pools`

    create a new pool, return a redirect to /pools/<id>


* `PUT    /pools/<id>`

    update a pools properties or even the whole tasks section


* `DELETE /pools/<id>`

    delete a pool (set the deleted flag)

----

* `GET    /pools/<id>/tasks`
    
    return the tasks of one pool

* `POST   /pools/<id>/tasks`

    add a new task to a pool

* `PUT    /pools/<id>/tasks/<id>`

    update a single task in a pool

* `DELETE /pools/<id>/tasks/<id>`

    remove a task from a pool

Exams
-----

* `GET    /exams`

    return all exams

    ?all=<true|false|1|0> 

* `POST   /exams`

    create a new exam

* `DELETE /exams/<id>`

    set the deleted flag on an exam

Audits
------

* `GET    /audits`

    return all audits

* `POST   /audits`

    create a new audit, the initial status is 'registered'

* `PUT   /audits`

    the only thing one can change is the 'status'
    fsm: registered => started => finished => corrected (=> frozen)

* `DELETE /audits/<id>`

Trials
------

* `GET    /trials`

    return all trials

* `GET    /trials/<id>`

    return a specific trial

* `PUT    /trials/<id>`

    update a trial document - this is only possible when audit.status == 'started'
    or when it is 'finished'




(approx 22 endpoints)

An audit has different stages:

1. registered
2. started
3. finished
4. corrected

A user has an id, email, first name and last name and belongs to a group.
Group membership determine permissions: guest, editor, admin.

Pools and trials are documents.
Exams and audits are relations.

Pools and trials are documents, since they contain they are flexible and nested.

Exams and audits are relational.



API vX
------

/pools
/pools/<id>

/exams/<id>

/exams/<id>/audits
/exams/<id>/audits/<id>

/exams/<id>/audits/<id>/trials
/exams/<id>/audits/<id>/trials/<id>


POST /pools
{
    "id" : "geo101",
    "tasks" : [...]
}

POST /pools/1/tasks
{
    "kind" : "cloze",
    "id" : "...",
}

POST /exams
{
    "pool_id" : "geo101",
    "name" : "Fall 2012",
}

POST /exams/1/audits
{
    "user_id" : 1
}

PUT /exams/1/audits/1
{
    "status" : "started"
}

PUT /exams/1/audits/1
{
    "status" : "started"
}

PUT /trials/2343
{
    "id" : "...",
    "answers" : {}
}

Pools sind einfach. Zweifach geschachtelt.
Man kann ein Exam erstellen. Dieses Exam muss einen Pool nutzen.


API v2
------

Three main resources: pools, exams, audits.
