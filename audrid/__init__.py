# coding: utf-8
"""
The main application.
Include MethodRewriteMiddleware to piggyback PUT/DELETE on POST.
"""
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import url_decode

class MethodRewriteMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if 'METHOD_OVERRIDE' in environ.get('QUERY_STRING', ''):
            args = url_decode(environ['QUERY_STRING'])
            method = args.get('__METHOD_OVERRIDE__')
            if method:
                method = method.encode('ascii', 'replace')
                environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://audrid:audrid@localhost/audrid'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/audrid.db'
app.config['ES_HOST'] = 'localhost'
app.config['ES_PORT'] = '9200'
app.config['SECRET_KEY'] = 'sdasd78sadhjahsd7ll8sdL%$§%<f6776ad328989=)(&%'
app.wsgi_app = MethodRewriteMiddleware(app.wsgi_app)
db = SQLAlchemy(app)

from views import *
