#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function

from audrid import (app, db, models)
from flask.ext.script import Manager, prompt_bool
import pyes
import base64
import sys
import json

manager = Manager(app)

@manager.command
def create_db():
    """ Create the database.
    """
    db.create_all(app=app)

@manager.command
def drop_db():
    """ Drop the database.
    """
    if prompt_bool("Are you sure you want to lose all your data"):
        db.drop_all(app=app)

@manager.command
def drop_index():
    """ Purge ES index.
    """
    try:
        es = pyes.ES('127.0.0.1:9200')
        es.delete_index_if_exists('audrid')
    except Exception as exc:
        print(exc, file=sys.stderr)

def create_index_mapping():
    pass

@manager.command
def create_sample_users():  
    """ Create admin and guest user.
    """
    try:
        admin = models.User(username='admin', email='martin.czygan@gmail.com', password='admin', group='admin')
        guest = models.User(username='guest', email='guest@example.com', password='guest')
        db.session.add_all([admin, guest])
        db.session.commit()
        print('added admin:admin')
        print('added guest:guest')
    except Exception as exc:
        db.session.rollback()
        print(exc, file=sys.stderr)

@manager.command
def load_fixtures():
    """ Create sample business data.
    """
    test_app = app.test_client()
    def open_with_auth(url, method='GET', username='admin', password='admin', data=None):
        headers = {
            'Authorization' : 'Basic %s' % (
            base64.b64encode("%s:%s" % (username, password)))
        }
        print('%s\t%s\t%s' % (url, method, data))
        return test_app.open(url, method=method, headers=headers, data=data)

    open_with_auth('/api/v1/pools', method='POST', data=json.dumps({'id' : u'123456', 'tasks' : []}))
    # open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"pool_id" : u'123456', 'name' : u'This is a test pool'}))
    # open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))


if __name__ == "__main__":
    manager.run()
