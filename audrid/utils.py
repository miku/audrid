# coding: utf-8

from flask import (request, jsonify, session)
from functools import wraps
import datetime
import random
import string

def random_id(length=6):
    """ Return a random char identifier. 
    """
    return u''.join([ random.choice(
        string.lowercase + string.digits) for _ in range(length) ])

def random_id_fun(length=6):
    """ Return a random id generator.
    """
    def inner():
        return random_id(length=length)
    return inner

def isodate(value):
    """ Iso date as string for your convenience. """
    return datetime.datetime.now().isoformat()

def authorization_required(f):
    """ Simple HTTP Basic auth decorator.
    """
    from audrid.models import User

    @wraps(f)
    def decorated_function(*args, **kwargs):
        try:
            username = request.authorization.username
            user_obj = User.query.filter_by(username=username).first()
            if not user_obj.password == request.authorization.password:
                return jsonify({"message" : "Not authorized"}), 401
        except Exception as exc:
            return jsonify({"message" : "Not authorized"}), 401
        kwargs.update({"user" : user_obj})
        return f(*args, **kwargs)
    return decorated_function