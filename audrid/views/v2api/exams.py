# coding: utf-8

from audrid import (app, db)
from audrid import validators as V
from audrid.models import (User, Exam, Audit, Pool)
from audrid.utils import (random_id, authorization_required)
from flask import (jsonify, request, redirect, url_for)
from functools import wraps
import datetime
import json

#
# GET /exams
# GET /exams/<id>
#
@app.route('/api/v2/exams', methods=['GET'])
@authorization_required
def exams_get_all(user=None):
    """ Return all exams. """
    return jsonify(exams=[ e.as_dict() for e in Exam.query.filter_by(deleted=False).all() ])

@app.route('/api/v2/exams/<id>', methods=['GET'])
@authorization_required
def exams_get_one(user=None):
    """ Get one exam. """
    exam = Exam.query.filter_by(id=id).filter_by(deleted=False).first_or_404()
    return jsonify(exam.as_dict())

#
# POST /exams
#
@app.route('/api/v2/exams', methods=['POST'])
@authorization_required
def exams_get_post(user=None):
    """ Return all exams. """
    try:
        __exam = V.exam(json.loads(request.data))
    except Exception as exc:
        return jsonify(message='error during validation: %s' % exc), 400
    _ = Pool.query.filter_by(id=__exam['pool_id']).first_or_404()
    try:
        exam = Exam(**__exam)
        db.session.add(exam)
        db.session.commit()
    except Exception as exc:
        db.session.rollback()
        print(exc)
        return jsonify(message='storage error: %s' % exc), 400
    return jsonify(exam.as_dict())

#
# DELETE /exams/<id>
#
@app.route('/api/v2/exams/<id>', methods=['DELETE'])
@authorization_required
def exams_delete_one(id, user=None):
    """ Delete one exam. """
    exam = Exam.query.filter_by(id=id).first_or_404()
    exam.deleted = True
    db.session.add(exam)
    db.session.commit()
    return jsonify(exam.as_dict())

