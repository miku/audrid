# coding: utf-8

from audrid import (app, db)
from audrid import validators as V
from audrid.models import (User, Exam, Audit, Pool, PoolVersion)
from audrid.utils import (random_id, authorization_required)
from flask import (jsonify, request, redirect, url_for)
from functools import wraps
import datetime
import json

#
# GET /pools
# GET /pools/<id>
# GET /pools/<id>/tasks
# GET /pools/<pid>/tasks/<tid>
#
@app.route('/api/v2/pools', methods=['GET'])
@authorization_required
def pools_get_all(user=None):
    """
    Just return all (not deleted) pools.
    """
    pools = Pool.query.filter_by(deleted=False)
    return jsonify(pools=[ json.loads(p.body) for p in pools ])

@app.route('/api/v2/pools/<id>', methods=['GET'])
@authorization_required
def pools_get(id, user=None):
    """
    Get a specific pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=id).first_or_404()
    return jsonify(json.loads(pool.body))

@app.route('/api/v2/pools/<id>/tasks', methods=['GET'])
@authorization_required
def pools_tasks_get_all(id, user=None):
    """
    Return just the tasks of a pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=id).first_or_404()
    return jsonify(tasks=pool.tasks())

@app.route('/api/v2/pools/<pid>/tasks/<tid>', methods=['GET'])
@authorization_required
def pools_tasks_get(pid, tid, user=None):
    """
    Return just a single tasks from a pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=pid).first_or_404()
    task = pool.tasks(id=tid)
    if task == None:
        return jsonify(message='no such task: %s' % tid), 404
    return jsonify(task)

@app.route('/api/v2/pools/<id>/versions', methods=['GET'])
@authorization_required
def pools_versions_get_all(id, user=None):
    """
    Return the versions of a pool
    """
    _ = Pool.query.filter_by(id=id).first_or_404()
    pool_versions = PoolVersion.query.filter_by(id=id).order_by('version DESC').all()
    return jsonify(versions=[ pv.as_dict() for pv in pool_versions ])

@app.route('/api/v2/pools/<id>/versions/<version>', methods=['GET'])
@authorization_required
def pools_versions_get_one(id, version, user=None):
    """
    Return the versions of a pool
    """
    _ = Pool.query.filter_by(id=id).first_or_404()
    pool_version = PoolVersion.query.filter_by(id=id).filter_by(version=version).first_or_404()
    return jsonify(pool_version.as_dict())

#
# POST /pools
# POST /pools/<id>/tasks
#
@app.route('/api/v2/pools', methods=['POST'])
@authorization_required
def pools_post(user=None):
    """
    Create a new pool.
    """
    try:
        __pool = V.pool(json.loads(request.data))
    except Exception as exc:
        return jsonify(message='error during validation: %s' % exc), 400

    try:
        pool = Pool(id=__pool['id'], body=json.dumps(__pool))
        db.session.add(pool)
        db.session.flush()
        version = PoolVersion(id=pool.id, body=pool.body, updated_by=user.id)
        db.session.add(version)
        db.session.commit()        
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400
    return redirect(url_for('pools_get', id=pool.id))

@app.route('/api/v2/pools/<id>/tasks', methods=['POST'])
@authorization_required
def pools_tasks_post(id, user=None):
    """
    Add a task to a pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=id).first_or_404()

    try:
        task = V.task(json.loads(request.data))
    except Exception as exc:
        return jsonify(message='error during validation: %s' % exc), 400    

    try:
        pool.append_task(task)
        db.session.add(pool)
        version = PoolVersion(id=pool.id, body=pool.body, updated_by=user.id)
        db.session.add(version)
        db.session.commit()
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400    
    return jsonify(task=task)

#
# DELETE /pools
# DELETE /pools/<id>
# DELETE /pools/<id>/tasks
# DELETE /pools/<pid>/tasks/<tid>
#
@app.route('/api/v2/pools/<pid>/tasks/<tid>', methods=['DELETE'])
@authorization_required
def pools_delete_tasks_one(pid, tid, user=None):
    """
    Delete one task from pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=pid).first_or_404()
    try:
        pool.remove_task(tid)
        db.session.add(pool)
        version = PoolVersion(id=pool.id, body=pool.body, updated_by=user.id)
        db.session.add(version)        
        db.session.commit()
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400        
    return jsonify(pool.as_dict())

@app.route('/api/v2/pools/<id>/tasks', methods=['DELETE'])
@authorization_required
def pools_delete_tasks_all(id, user=None):
    """
    Delete all tasks from a pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=id).first_or_404()
    try:
        content = json.loads(pool.body)
        content['tasks'] = []
        pool.body = json.dumps(content)
        version = PoolVersion(id=pool.id, body=pool.body, updated_by=user.id)
        db.session.add(version)        
        db.session.add(pool)
        db.session.commit()
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400        
    return jsonify(pool.as_dict())

@app.route('/api/v2/pools/<id>', methods=['DELETE'])
@authorization_required
def pools_delete(id, user=None):
    """
    Delete a specific pool.
    """
    pool = Pool.query.filter_by(deleted=False).filter_by(id=id).first_or_404()
    try:
        pool.deleted = True
        db.session.add(pool)
        db.session.commit()
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400        
    return jsonify({'id' : pool.id, 'deleted' : True})

@app.route('/api/v2/pools', methods=['DELETE'])
@authorization_required
def pools_delete_all(user=None):
    """
    Set deleted flag for all pools to true.
    """
    try:
        pools = Pool.query.filter_by(deleted=False)
        for i, pool in enumerate(pools):
            pool.deleted = True
            db.session.add(pool)
            db.session.commit()
    except Exception as exc:
        db.session.rollback()
        return jsonify(message='storage error: %s' % exc), 400
    return jsonify({'id' : [ p.id for p in pools], 'deleted' : True})

