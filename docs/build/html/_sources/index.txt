This is the documentation for Audrid, an experimental e-assessment platform.

.. toctree::
   :maxdepth: 2

   intro
   models
   validators
   api
   testing
   frontend
   misc