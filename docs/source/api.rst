API Reference
=============

Users
-----

Users are minimal at the moment. They come with a unique username, a unique email
and a group, which is just a string, which can be one of:

* Guest
* Editor
* Admin

Pools
-----

Pools are the content-backbone of tests. 

Exams
-----

Exams are what candidates like to take.

Audits
------

Audits represent the actions of an candidate during a test.
