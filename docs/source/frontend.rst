Frontend
========

The frontend uses a responsive design. The following screens have been implemented:

* Registration
* Login
* Request Password Reset
* Password Reset
* Index (default screen when not logged in)

For admins:

* Home (default, when logged in, with Activity Feed)
* Create Pool
* Edit Pool
* Create Exam
* Show Audits (Registrations)

For editor:

* Home (default, when logged in, Activity Feed, Show uncorrected audits)
* Correct Audit

For guests:

* Show available exams
* Register for exam
* The audit-in-progress screen
