Introduction
============

Audrid is an experimental web-based e-assessment application. It tries to say
no to overweight task specifications and to embrace a document-oriented
approach, while staying as sane as possible. And yes, it strives to offer a
fully equipped REST-API as well.
