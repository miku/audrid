Miscellaneous
=============


Scriptability
-------------

The API provides the basis for scriptability. Lets say we
obtained a text file with countries and capitals. We 
want to create a test out of this data. This could be
done simple on a UNIX system with some standard tools installed:

::

    $ head -3 capitals.txt # data is tab-separated
    Berlin Germany
    Moscow Russia
    Japan Tokyo
    ...

And as script:

::

    #!/usr/bin/env bash

    curl -XPOST localhost:5000/api/v2/pools 
        -H 'Content-Type: application/json'
        -d '{"id" : "capitals-game", "tasks" : []}'

    for line in capitals.txt; do
        
        CAPITAL=$(awk '{print $1}')
        COUNTRY=$(awk '{print $2}')
        TASK_ID=$(echo /dev/urandom|head -64|md5|cut -c 1-8)

        curl -XPOST localhost:5000/api/v2/pools/capitals-game/tasks
            -H 'Content-Type: application/json'
            -d '{
                "id" : "$TASK_ID", 
                "kind" : "single", 
                "task" : "What is the capital of $COUNTRY", 
                "answer" : "$CITY"
            }'

    done

And done.
