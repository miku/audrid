Models
======

There are three domain models in the system:

* Pools
* Exams
* Audits

Furthermore there are models for essential services,
such as

* Users
* PoolVersions
* Log

The finer grained models for pools, tasks and answers
are specified in a document-oriented manner. These
objects are explained in the validators chapter.
