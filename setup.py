# coding: utf-8

from distutils.core import setup
import setuptools

setup(
    name='audrid',
    version='0.1.0',
    author='Martin Czygan',
    author_email='martin.czygan@gmail.com',
    packages=['audrid'],
    # scripts=['bin/ezri'],
    # license='LICENSE.md',
    description='Audrid Experimental E-Assessment',
    long_description='Audrid Experimental E-Assessment',
)
