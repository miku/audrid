# coding: utf-8

from audrid.models import (
    Pool,
    User,
    Exam,
    Audit,
)

from audrid import (app, db, utils)
import base64
import json
import time
import unittest

class AuditAPITests(unittest.TestCase):
    """ Test DB.
    """
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = True
        app.config['CSRF_ENABLED'] = False
        # app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://audrid:audrid@localhost/audrid_test'
        self.app = app.test_client()
        db.drop_all(app=app)
        db.create_all(app=app)

        admin = User(username='admin', email='martin.czygan@gmail.com', password='admin')
        guest = User(username='guest', email='guest@example.com', password='guest')
        db.session.add(admin)
        db.session.add(guest)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all(app=app)

    def open_with_auth(self, url, method='GET', username='admin', password='admin', data=None, follow_redirects=False):
        headers = {
            'Authorization' : 'Basic %s' % (
            base64.b64encode("%s:%s" % (username, password)))
        }
        return self.app.open(url, method=method, headers=headers, data=data, follow_redirects=follow_redirects)

    def test_audits_get_all_returns_200(self):
        """ GET    /audits should return 200 """
        rv = self.open_with_auth('/api/v2/audits', method='GET')
        self.assertEqual(200, rv.status_code)

    def test_audits_get_all_returns_correct_number(self):
        """ GET    /audits should return correct number of audits """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'123', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "123"}))

        rv = self.open_with_auth('/api/v2/audits', method='GET')
        self.assertEqual(0, len(json.loads(rv.data).get('audits', [])))

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))

        rv = self.open_with_auth('/api/v2/audits', method='GET')
        self.assertEqual(1, len(json.loads(rv.data).get('audits', [])))

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))

        rv = self.open_with_auth('/api/v2/audits', method='GET')
        self.assertEqual(2, len(json.loads(rv.data).get('audits', [])))

    def test_audits_get_all_has_audits_key(self):
        """ GET    /audits returns dict with 'audits' key """
        rv = self.open_with_auth('/api/v2/audits', method='GET')
        self.assertTrue('audits' in json.loads(rv.data))

    def test_audits_get_one_throws_404_on_missing_audit(self):
        """ GET    /audits/<id> returns 404 on miss """
        rv = self.open_with_auth('/api/v2/audits/123', method='GET')
        self.assertEqual(404, rv.status_code)

    def test_audits_post_rejects_non_existing_exam_id(self):
        """ POST   /audits rejects non existent exam_id """
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({
            "exam_id" : 1123, "user_id" : 1 }))
        self.assertEqual(404, rv.status_code)

    def test_audits_post_rejects_non_existing_user_id(self):
        """ POST   /audits rejects non existent user_id """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'1234', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "1234"}))
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({
            "exam_id" : 1, "user_id" : 123 }))
        self.assertEqual(404, rv.status_code)

    def test_audits_post_rejects_invalid_data(self):
        """ POST   /audits rejects invalid data """
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps("Hello"))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps([]))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : "Hello World"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1.0}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"user_id" : "Hello"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"user_id" : 1, "exam_id" : "nono"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : "1", "user_id" : "2"}))
        self.assertEqual(400, rv.status_code)

    def test_audits_post_accepts_correct_data(self):
        """ POST   /audits accepts correct data """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'123', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "123"}))

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 2}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 2}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 2}))
        self.assertEqual(200, rv.status_code)

    def test_audits_put_rejects_invalid_data(self):
        """ PUT    /audits rejects invalid data """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'123', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "123"}))
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 2, "user_id" : 1}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 2}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({}))
        self.assertEqual(400, rv.status_code)

    def test_audits_put_rejects_invalid_status_transitions(self):
        """ PUT    /audits rejects invalid status transitions """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'123', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "123"}))
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "finished"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "corrected"}))
        self.assertEqual(400, rv.status_code)

    def test_audits_put_accept_valid_status_transitions(self):
        """ PUT    /audits accepts valid status transitions """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'123', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "123"}))
        rv = self.open_with_auth('/api/v2/audits', method='POST', data=json.dumps({"exam_id" : 1, "user_id" : 1}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "registered"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "started"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "started"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "finished"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "finished"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "corrected"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/audits/1', method='PUT', data=json.dumps({"exam_id" : 1, "user_id" : 1, "status" : "corrected"}))
        self.assertEqual(200, rv.status_code)
