# coding: utf-8

from audrid.models import (
    Pool,
    User,
    Exam,
)

from audrid import (app, db, utils)
import base64
import json
import time
import unittest

class ExamsAPITests(unittest.TestCase):
    """ Test DB.
    """
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = True
        app.config['CSRF_ENABLED'] = False
        # app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://audrid:audrid@localhost/audrid_test'
        self.app = app.test_client()
        db.drop_all(app=app)
        db.create_all(app=app)

        admin = User(username='admin', email='martin.czygan@gmail.com', password='admin')
        guest = User(username='guest', email='guest@example.com', password='guest')
        db.session.add(admin)
        db.session.add(guest)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all(app=app)

    def open_with_auth(self, url, method='GET', username='admin', password='admin', data=None, follow_redirects=False):
        headers = {
            'Authorization' : 'Basic %s' % (
            base64.b64encode("%s:%s" % (username, password)))
        }
        return self.app.open(url, method=method, headers=headers, data=data, follow_redirects=follow_redirects)

    def test_exam_post_rejects_invalid_data(self):
        """ POST   /exams should reject invalid data """
        rv = self.open_with_auth('/api/v2/exams', method='POST')
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"id" : 10}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps([]))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps("Hello"))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"pool_id" : 1}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "sample exam"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "", "pool_id" : "1"}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "12", "krakra" : 123}))
        self.assertEqual(400, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps(
            {"name" : "test", "pool_id" : "12", "duration" : "hello" }))
        self.assertEqual(400, rv.status_code)

    def test_exam_post_fails_on_missing_pool(self):
        """ POST   /exams should fail if pool_id does not exist """
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps(
            {"name" : "iazsdu", "pool_id" : "12", "duration" : 50, "description" : "this is a sample test"}))
        self.assertEqual(404, rv.status_code)

    def test_exam_post_accept_valid_data(self):
        """ POST   /exams should accept valid data """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'12', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test-1", "pool_id" : "12"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps(
            {"name" : "test-2", "pool_id" : "12", "description" : "Hello World"}))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps(
            {"name" : "test-3", "pool_id" : "12", "duration" : 50, }))
        self.assertEqual(200, rv.status_code)

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps(
            {"name" : "test-4", "pool_id" : "12", "duration" : 50, "description" : "this is a sample test"}))
        self.assertEqual(200, rv.status_code)

    def test_exam_get_all_200(self):
        """ GET    /exams should return 200 """
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(200, rv.status_code)

    def test_exam_get_all_is_empty(self):
        """ GET    /exams should return an 0-len list """
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(200, rv.status_code)
        self.assertEqual(0, len(json.loads(rv.data).get('exams', None)))

    def test_exam_get_all_correct_number_of_exams(self):
        """ GET    /exams should return the correct number of exams """
        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'1234', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "1234"}))
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(200, rv.status_code)
        self.assertEqual(1, len(json.loads(rv.data).get('exams', None)))

        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test-2", "pool_id" : "1234"}))
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(200, rv.status_code)
        self.assertEqual(2, len(json.loads(rv.data).get('exams', None)))

    def test_exam_get_all_is_empty(self):
        """ DELETE /exams/<id> should set the deleted flag """
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(0, len(json.loads(rv.data).get('exams', None)))

        rv = self.open_with_auth('/api/v2/pools', method='POST', data=json.dumps({'id' : u'1234', 'tasks' : []}))
        rv = self.open_with_auth('/api/v2/exams', method='POST', data=json.dumps({"name" : "test", "pool_id" : "1234"}))
        
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(1, len(json.loads(rv.data).get('exams', None)))

        rv = self.open_with_auth('/api/v2/exams/1', method='DELETE')
        rv = self.open_with_auth('/api/v2/exams', method='GET')
        self.assertEqual(0, len(json.loads(rv.data).get('exams', None)))

