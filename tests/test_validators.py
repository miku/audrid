# coding: utf-8

from voluptuous import InvalidList
from audrid.validators import (
    cloze,
    text,
    single,
    multiple_choice,
    mapping,
    pool,
    trial,
)
import unittest
import json
import os

def openrel(filename):
    """ Return a handle on a filename, relative to this test file and don't care
        where this is called from.
    """
    return open(os.path.join(os.path.abspath(
        os.path.dirname(__file__)), filename))

class FilelistMixin(unittest.TestCase):
    """
    Provide ``should_pass_on`` and ``should_fail_on``
    methods. 
    """
    def should_pass_on(self, filenames, schema=None):
        """
        Loads each filename in ``filenames`` and validates against ``schema``.
        """
        for fn in filenames:
            with openrel(fn) as handle:
                content = json.load(handle)
                self.assertEqual(content, schema(content))

    def should_fail_on(self, filename_exc_tuples, schema=None):
        """
        Takes a list of (filename, exception) tuples and ensures,
        that the exceptions are actually raised while validating against ``schema``.
        """
        for fn, exc in filename_exc_tuples:
            with self.assertRaises(exc):
                with openrel(fn) as handle:
                    schema(json.load(handle))

class TaskValidatorTests(FilelistMixin):

    def test_cloze(self):
        """ V.cloze
        """
        valid_files = [
            "documents/cloze0.json",
            "documents/cloze3.json",
        ]
        invalid_files = [
            ("documents/cloze1.json", InvalidList),
            ("documents/cloze2.json", InvalidList),
            ("documents/cloze4.json", InvalidList),
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=cloze)
        self.should_fail_on(invalid_files, schema=cloze)

    def test_text(self):
        """ V.text
        """
        valid_files = [
            "documents/text0.json",
        ]
        invalid_files = [
            ("documents/text1.json", InvalidList),
        ]

        self.should_pass_on(valid_files, schema=text)
        self.should_fail_on(invalid_files, schema=text)

    def test_mapping(self):
        """ V.mapping
        """
        valid_files = [
            "documents/mapping0.json",
        ]
        invalid_files = [
            ("documents/mapping1.json", InvalidList),
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=mapping)
        self.should_fail_on(invalid_files, schema=mapping)

    def test_single(self):
        """ V.single
        """
        valid_files = [
            "documents/single0.json",
        ]
        invalid_files = [
            ("documents/single1.json", InvalidList),
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=single)
        self.should_fail_on(invalid_files, schema=single)

    def test_multiple_choice(self):
        """ V.multiple_choice
        """
        self.maxDiff = None
        valid_files = [
            "documents/mc0.json",
        ]
        invalid_files = [
            ("documents/mc1.json", InvalidList),
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=multiple_choice)
        self.should_fail_on(invalid_files, schema=multiple_choice)

class PoolValidatorTests(FilelistMixin):

    def test_pool(self):
        """ V.pool
        """
        valid_files = [
            "documents/pool0.json",
            "documents/pool2.json",
            "documents/pool4.json",
        ]
        invalid_files = [
            ("documents/pool1.json", InvalidList),
            ("documents/pool3.json", InvalidList),
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=pool)
        self.should_fail_on(invalid_files, schema=pool)

class AuditValidatorTests(FilelistMixin):

    def test_audit(self):
        """ V.audit
        """        
        valid_files = [
            "documents/audit0.json",
        ]
        invalid_files = [
            ("documents/empty.json", ValueError),
        ]

        self.should_pass_on(valid_files, schema=trial)
        self.should_fail_on(invalid_files, schema=trial)
